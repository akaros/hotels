package controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategorieChambre;
import beans.Departement;
import beans.TypeHotel;
import core.List;
import daos.HotelDAO;
import daos.TypeHotelDAO;
import tools.Misc;
import daos.CategorieChambreDAO;
import daos.DepartementDAO;

/* SERVLET */

@WebServlet( name="MainServlet", urlPatterns = "/search" )
public class SearchServlet extends HttpServlet {
	
	/* ATTRIBUTES */
	
	private static final long serialVersionUID = 1L;
	private boolean                connected;
	private HotelDAO               hotelDAO;
	private TypeHotelDAO           typeHotelDAO;
	private CategorieChambreDAO    categorieChambreDAO;
	private DepartementDAO         departementDAO;
	private List<TypeHotel>        typesHotels;
	private List<CategorieChambre> categories;
	private List<Departement>      departements;
	private List<Integer>          countFromTypesHotels;
	private List<Integer>          countFromCategoriesChambres;
	private List<Integer>          countFromDepartements;
       
    /* METHODS */
	
    public SearchServlet() {
        super();
    }
    
    /* La m�thode init est appell� une fois lors de l'instanciation (unique) de la servlet */
    public void init(ServletConfig conf) {
    	
    	// Connexion � Oracle
    	connected = tools.ConnectionManager.init("hotel", "afpa123");
    	
    	// Cr�ation des DAO
    	hotelDAO = new HotelDAO();
    	typeHotelDAO = new TypeHotelDAO();
    	categorieChambreDAO = new CategorieChambreDAO();
    	departementDAO = new DepartementDAO();
    	
    	try {
    		// On r�cup�re les listes de filtres
			typesHotels = typeHotelDAO.findAll();
			categories = categorieChambreDAO.findAll();
			departements = departementDAO.findAll();
			
			// Et le compte en terme d'hotels de chaque filtre
			countFromTypesHotels = hotelDAO.countFromTypes(typesHotels.size());
			countFromCategoriesChambres = hotelDAO.countFromCategoriesChambres(categories.size());
			countFromDepartements = hotelDAO.countFromDepartements(departements.size());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	/* GET HANDLER */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String num = request.getParameter("num");
		String page = request.getParameter("page");
		
		if( num != null ) {
			HttpSession session = request.getSession();
			try {
				session.setAttribute("currentHotel", hotelDAO.find(num));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if( page != null ) {
			
			request.setAttribute("page", page);
			
			try {
				request.setAttribute("start", Integer.parseInt(page) * 100);
			} catch( Exception e ) {
				
			}
			
		}
		
		process(request, response);
	}

	/* POST HANDLER */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			// R�cup�ration des filtres r�cup�r�s en param�tres de la requ�te et enregistrement dans la session
			HttpSession session = request.getSession();
			session.setAttribute("results", hotelDAO.findByCriteria(
					Misc.convertToIntArray(request.getParameterValues("typs")),
					Misc.convertToIntArray(request.getParameterValues("cats")),
					Misc.convertToIntArray(request.getParameterValues("deps"))
			));
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		process(request, response);
	}
	
	/* BOTH GET AND POST HANDLER */
	protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("types", typesHotels.getCollection());
		request.setAttribute("categories", categories.getCollection());
		request.setAttribute("departements", departements.getCollection());
		request.setAttribute("typesCount", countFromTypesHotels);
		request.setAttribute("categoriesCount", countFromCategoriesChambres);
		request.setAttribute("departementsCount", countFromDepartements);		
		
		request.getServletContext().getRequestDispatcher("/search.jsp").forward(request, response);
	}
}
