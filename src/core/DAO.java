package core;

/* INTERFACE DECRIVANT LES METHODES CRUD D'ACCES ET DE MANIPULATION DES DONNEES DISTANTES */

public interface DAO<T> {
	
	/* METHODS */
	
	public T create(T object)    throws Exception; // Cr�ation d'un objet dans la BDD distante
	public T find(Object id)     throws Exception; // R�cup�ration d'un objet dans la BDD distante
	public List<T> findAll()     throws Exception; // R�cup�rations de plusieurs objets dans la BDD distante
	public T update(T object)    throws Exception; // Mise � jour d'un objet dans la BDD distante
	public void delete(T object) throws Exception; // Suppression d'un objet dans la BDD distante
}