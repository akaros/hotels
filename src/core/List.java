package core;

import java.util.ArrayList;
import java.util.Iterator;

/* WRAPPER TO A COLLECTION */
public class List<T> implements Iterable<T> {
	
	/* ATTRIBUTES */
	
	private ArrayList<T> collection;
	
	/* METHODS */
	
	public List() {
		this.collection = new ArrayList<T>();
	}
	
	public void add(T object) {
		this.collection.add(object);
	}
	
	public int size() {
		return collection.size();
	}
	
	public T get(int indice) {
		return collection.get(indice);
	}
	
	public void clear() {
		collection.clear();
	}
	
	public void remove(int indice) {
		collection.remove(indice);
	}
	
	public void remove(T object) {
		collection.remove(object);
	}
	
	public Iterator<T> iterator() {
		return collection.iterator();
	}
	
	public String toString() {
		String str = "";
		for( T object : collection ) {
			str = str + object +"\n";
		}
		return str;
	}
	
	public final ArrayList<T> getCollection() {
		return collection;
	}
}