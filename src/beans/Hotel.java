package beans;

import tools.Misc;

public class Hotel {
	
	// Attributes
	
	private Integer num;
	private String  nom;
	private Integer type;
	private String  adresse;
	private String  cp;
	private String  ville;
	
	// Constructor
	
	public Hotel() {
		
		num = null;
		nom = null;
		type = null;
		adresse = null;
		cp = null;
		ville = null;
	}
	
	public Hotel(Integer num , String nom, Integer type, String adresse, String cp, String ville) {
		
		setNum(num);
		setNom(nom);
		setType(type);
		setAdresse(adresse);
		setCP(cp);
		setVille(ville);
	}

	// Methods
	
	public String toString() {
		
		return "Hotel :\n- num : " + num + "\n- nom : " + nom + "\n- adresse : " + adresse +
				"\n- " + cp + " " + ville;
	}

	// Getters
	
	public Integer getNum() {
		
		return this.num;
	}

	public String getNom() {
		
		return this.nom;
	}

	public String getAdresse() {
		
		return this.adresse;
	}

	public String getCP() {
		
		return this.cp;
	}

	public String getVille() {
		
		return this.ville;
	}
	
	public Integer getType() {
		
		return this.type;
	}
	
	// Setters
	
	public void setType(Integer type) {
		
		this.type = type;
	}

	public void setNum(Integer num) {
		
		this.num = num;
	}

	public void setNom(String nom) {
		
		this.nom = Misc.titleCase(nom);
	}
	
	public void setAdresse(String adresse) {
		
		this.adresse = Misc.titleCase(adresse);
	}
	
	public void setVille(String ville) {
		
		this.ville = Misc.titleCase(ville);
	}
	
	public void setCP(String cp) {
		
		this.cp = cp;
	}
}
