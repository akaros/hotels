package beans;

/* CATEGORIE DE CHAMBRE (IMMUABLE) */

public class CategorieChambre {
	
	/* ATTRIBUTES */
	
	private final int id;
	private final String name;
	
	/* METHODS */
	
	public CategorieChambre(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return id + " : " + name;
	}
}
