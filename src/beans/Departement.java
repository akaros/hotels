package beans;

/* REPRESENTS A DEPARTEMENT (IMMUTABLE) */

public class Departement {

	/* ATTRIBUTES */
	
	private static String[] NOMS = {
			
		"Ain", "Aisne", "Allier", "Alpes-de-Haute-Provence", "Hautes-Alpes", "Alpes-Maritimes", "Ard�che", "Ardennes",
		"Ari�ge", "Aube", "Aude", "Aveyron", "Bouches-du-Rh�ne", "Calvados", "Cantal", "Charente", "Charente-Maritime",
		"Cher", "Corr�ze", "Corse-du-Sud", "Haute-Corse", "C�te-d'Or", "C�tes-d'Armor", "Creuse", "Dordogne", "Doubs",
		"Dr�me", "Eure", "Eure-et-Loir", "Finist�re", "Gard", "Haute-Garonne", "Gers", "Gironde", "H�rault",
		"Ille-et-Vilaine", "Indre", "Indre-et-Loire", "Is�re", "Jura", "Landes", "Loir-et-Cher", "Loire", "Haute-Loire",
		"Loire-Atlantique", "Loiret", "Lot", "Lot-et-Garonne", "Loz�re", "Maine-et-Loire", "Manche", "Marne", "Haute-Marne",
		"Mayenne", "Meurthe-et-Moselle", "Meuse", "Morbihan", "Moselle", "Ni�vre", "Nord", "Oise", "Orne", "Pas-de-Calais",
		"Puy-de-D�me", "Pyr�n�es-Atlantiques", "Hautes-Pyr�n�es", "Pyr�n�es-Orientales", "Bas-Rhin", "Haut-Rhin", "Rh�ne",
		"Haute-Sa�ne", "Sa�ne-et-Loire", "Sarthe", "Savoie", "Haute-Savoie", "Paris", "Seine-Maritime", "Seine-et-Marne",
		"Yvelines", "Deux-S�vres", "Somme", "Tarn", "Tarn-et-Garonne", "Var", "Vaucluse", "Vend�e", "Vienne",
		"Haute-Vienne", "Vosges", "Yonne", "Territoire de Belfort", "Essonne", "Hauts-de-Seine", "Seine-Saint-Denis",
		"Val-de-Marne", "Val-d'Oise"
	};
	
	private final int id;
	private final String name;
	
	/* METHODS */
	
	public Departement(int id) {
		this.id = id;
		this.name = nameById(id);
	}
	
	public String getName() {
		return nameById(id);
	}
	
	public int getId() {
		return id;
	}
	
	public static String nameById(int id) {
		return (id >= 0 && id < getTotalCount()) ? NOMS[id] : "";
	}
	
	public static int getTotalCount() {
		return NOMS.length;
	}
	
	public String toString() {
		return "" + id + " : " + name;
	}
}
