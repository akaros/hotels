package beans;

/* TYPE D'HOTEL (IMMUABLE) */

public class TypeHotel {
	
	/* ATTRIBUTES */
	
	private final int id;
	private final String name;
	
	/* METHODS */
	
	public TypeHotel(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return "" + id + " : " + name;
	}
}
