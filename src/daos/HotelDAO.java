package daos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import beans.Hotel;
import core.DAO;
import core.List;
import tools.ConnectionManager;
import tools.Misc;

/* HOTEL DAO */

public class HotelDAO implements DAO<Hotel> {
	
	/* ATTRIBUTES */
	
	private static final String CREATE_REQUEST     = "INSERT INTO HOTEL VALUES (?, ?, ?, ?, ?, ?)";
	private static final String DELETE_REQUEST     = "DELETE FROM HOTEL WHERE NUHOTEL = ?";
	private static final String SELECT_REQUEST     = "SELECT * FROM HOTEL";
	private static final String SELECT_ONE_REQUEST = "SELECT * FROM HOTEL WHERE NUHOTEL = ?";
	private static final String UPDATE_REQUEST     = "UPDATE HOTEL SET NOMHOTEL = ?, NUTYPE = ?, ADRESSE = ?, CODEPOST = ?, VILLE = ? "
												   + "WHERE NUHOTEL = ?";
	
	/* METHODS */
	
	public List<Integer> countFromTypes(int nbTypes) throws SQLException {
		
		List<Integer> results = new List<Integer>();
		Statement statement = ConnectionManager.getConnection().createStatement();
		for( int k = 0; k < nbTypes; k++ ) {
			try {
			
				ResultSet rs = statement.executeQuery("select count(*) from hotel where nutype = " + (k + 1));
				while( rs.next() ) {
					results.add(rs.getInt(1));
				}
				rs.close();
				
			} catch (SQLException e ) {
				System.out.println(e);
			}
		}
		
		statement.close();
		return results;
	}
	
	public List<Integer> countFromCategoriesChambres(int nbCategories) throws SQLException {
		
		List<Integer> results = new List<Integer>();
		Statement statement = ConnectionManager.getConnection().createStatement();
		
		for( int k = 0; k < nbCategories; k++ ) {
			int nb = 0;
			try {
				ResultSet rs = statement.executeQuery("select count(distinct nuhotel) from chambre where nucat = " + (k + 1));
				rs.next();
				results.add(rs.getInt(1));
				rs.close();
				
			} catch(SQLException e) {
				System.out.println(e);
			}
		}
		
		statement.close();
		return results;
	}
	
	public List<Integer> countFromDepartements(int nbDepartements) {
		
		List<Integer> results = new List<Integer>();
		Statement statement;
		try {
			statement = ConnectionManager.getConnection().createStatement();
			for( int k = 0; k < nbDepartements; k++ ) {
				int nb = 0;
				try {
					ResultSet rs = statement.executeQuery("select count(*) from hotel where floor(to_number(codepost)/1000) = " + (k + 1));
					rs.next();
					nb = rs.getInt(1);
					
				} catch(SQLException e) {
					System.out.println(e);
				}
				results.add(nb);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		return results;
	}

	@Override
	public Hotel create(Hotel hotel) throws Exception {
		/* Initialisation des objets */
		ResultSet results = null;
		PreparedStatement request = null;
		Hotel newHotel = null;
		/* Tentative de cr�ation de l'enregistrement dans la base */
		try {
			/* Cr�ation de la requ�te */
			request = Misc.initPreparedStatement(ConnectionManager.getConnection(), CREATE_REQUEST, false,
				hotel.getNum(), hotel.getNom(), hotel.getType(), hotel.getAdresse(), hotel.getCP(), hotel.getVille()
			);
			/* Envoi de la requ�te */
			int status = request.executeUpdate();
			/* Echec de cr�ation de l'enregistrement */
			if( status == 0 ) {
				throw new Exception( "�chec de la cr�ation du hotel en base, aucune ligne ajout�e dans la table." );
			}
			/*  R�cup�ration des cl�s g�n�r�es */
			results = request.getGeneratedKeys();
			/* Tentative d'attribution du code hotel g�n�r� par la base � l'objet m�tier */
	        if ( results.next() ) {
	        	int id = results.getInt(1);
	        	newHotel = this.find(id);
	        } else {
	            throw new Exception( "�chec de la cr�ation du hotel en base, aucun ID auto-g�n�r� retourn�." );
	        }
		} catch( SQLException e ) { e.printStackTrace();
		/* Fermeture silencieuse */
		} finally {
			results.close();
			request.close();
		}
		
		return newHotel;
	}

	@Override
	public Hotel find(Object id) throws Exception {
		/* Initialisation des objets */
		ResultSet results = null;
		PreparedStatement request = null;
		Hotel hotel = null;
		/* Tentative de modification de l'enregistrement dans la base */
		try {
			/* Cr�ation de la requ�te */
			request = Misc.initPreparedStatement(ConnectionManager.getConnection(), SELECT_ONE_REQUEST, true, id);
			/* Envoi de la requ�te */
			results = request.executeQuery();
			/* Analyse de la requ�te */
			if ( results.next() ) {
				hotel = new Hotel();
				hotel.setNum(results.getInt(1));
				hotel.setNom(results.getString(2));
				hotel.setType(results.getInt(3));
				hotel.setAdresse(results.getString(4));
				hotel.setCP(results.getString(5));
				hotel.setVille(results.getString(6));
			} else {
				throw new Exception("Pas d'enregistrement correspondant � cet id");
			}

		} catch( SQLException e ) { e.printStackTrace(); }
		return hotel;
	}
	
	@Override
	public List<Hotel> findAll() {
		/* Initialisation des objets */
		ResultSet results = null;
		PreparedStatement request = null;
		List<Hotel> hotels = new List<Hotel>();
		/* Tentative de modification de l'enregistrement dans la base */
		try {
			/* Cr�ation de la requ�te */
			request = Misc.initPreparedStatement(ConnectionManager.getConnection(), SELECT_REQUEST, true);
			/* Envoi de la requ�te */
			results = request.executeQuery();
			/* Analyse de la requ�te */
			while ( results.next() ) {
				
				Hotel hotel = new Hotel();
				hotel.setNum(results.getInt(1));
				hotel.setNom(results.getString(2));
				hotel.setType(results.getInt(3));
				hotel.setAdresse(results.getString(4));
				hotel.setCP(results.getString(5));
				hotel.setVille(results.getString(6));
				hotels.add(hotel);
			}

		} catch( SQLException e ) { e.printStackTrace(); }
		return hotels;
	}
	
	public ArrayList<Hotel> findByCriteria(ArrayList<Integer> tps, 						   // types d'h�tels
									  	   ArrayList<Integer> cts, 						   // cat�gories des chambres
									  	   ArrayList<Integer> dps  ) throws SQLException { // d�partements
		
		ArrayList<Hotel> hotels = new ArrayList<Hotel>();
		
		String cpString = "\n	WHERE (";
		
		for( int k = 0; k < dps.size(); k++ ) {
			
			cpString+=("floor(to_number(codepost)/1000) = " + dps.get(k) + ((k < dps.size() - 1) ? " OR " : ""));
		}
		
		cpString+=")\n";
		
		String tpString = (dps.size() == 0 ? "\n	WHERE (" : "\n	AND (");
		
		for( int k = 0; k < tps.size(); k++ ) {
			
			tpString+=("NUTYPE = " + tps.get(k) + ((k < tps.size() - 1) ? " OR " : ""));
		}
		
		tpString+=")\n";
		
		String ctString = "\n	WHERE (";
		
		for( int k = 0; k < cts.size(); k++ ) {
			
			ctString+=("NUCAT = " + cts.get(k) + ((k < cts.size() - 1) ? " OR " : ""));
		}
		
		ctString+=")\n";
		
		String query = "SELECT A.NUHOTEL, A.NOMHOTEL, A.NUTYPE, A.ADRESSE, A.CODEPOST, A.VILLE\n" +
					   "FROM (\n" +
					   "	SELECT *\n" +
					   "	FROM HOTEL\n" + (dps.size() == 0 ? "" : cpString) + (tps.size() == 0 ? "" : tpString) +
					   ") A INNER JOIN (\n" +
					   "	SELECT DISTINCT NUHOTEL\n" +
					   "	FROM CHAMBRE\n" + (cts.size() == 0 ? "" : ctString) +
					   ") B ON A.NUHOTEL = B.NUHOTEL";
		
		Statement statement = null;
		
		try {
			
			statement = ConnectionManager.getConnection().createStatement();
			ResultSet rs = statement.executeQuery(query);
			
			while( rs.next() ) {
				
				Hotel hotel = new Hotel();
				hotel.setNum(rs.getInt(1));
				hotel.setNom(rs.getString(2));
				hotel.setType(rs.getInt(3));
				hotel.setAdresse(rs.getString(4));
				hotel.setCP(rs.getString(5));
				hotel.setVille(rs.getString(6));
				hotels.add(hotel); // prevenir les observateurs
			}
		} catch (SQLException e ) {
			
			System.out.println(e);
		} finally {
			
			if( statement != null ) {
				statement.close();
			}
		}

		return hotels;
	}

	@Override
	public Hotel update(Hotel hotel) throws Exception {
		/* Initialisation des objets */
		PreparedStatement request = null;
		Hotel updatedHotel = null;
		/* Tentative de modification de l'enregistrement dans la base */
		try {
			/* Cr�ation de la requ�te */
			request = Misc.initPreparedStatement(ConnectionManager.getConnection(), UPDATE_REQUEST, true, 
					hotel.getNom(), hotel.getType(), hotel.getAdresse(), hotel.getCP(), hotel.getVille(), hotel.getNum()
			);
			/* Envoi de la requ�te */
			int status = request.executeUpdate();
			/* Echec de cr�ation de l'enregistrement */
			if( status == 0 ) {
				throw new Exception( "�chec de la maj de l'enregistrement." );
			} else {
	        	updatedHotel = this.find(hotel.getNum());
			}
		} catch( SQLException e ) { e.printStackTrace(); }
		return updatedHotel;
	}

	@Override
	public void delete(Hotel hotel) throws Exception {
		/* Initialisation des objets */
		PreparedStatement request = null;
		Hotel updatedHotel = null;
		/* Tentative de modification de l'enregistrement dans la base */
		try {
			/* Cr�ation de la requ�te */
			request = Misc.initPreparedStatement(ConnectionManager.getConnection(), DELETE_REQUEST, true, hotel.getNum());
			/* Envoi de la requ�te */
			int status = request.executeUpdate();
			/* Echec de cr�ation de l'enregistrement */
			if( status == 0 ) {
				throw new Exception( "�chec de la suppression de l'enregistrement." );
			}
		} catch( SQLException e ) { e.printStackTrace(); }
	}

	private static Hotel map(ResultSet res) throws SQLException {
		
	    return new Hotel(res.getInt(0), res.getString(1), res.getInt(2), res.getString(3), res.getString(4), res.getString(5));
	}
}
