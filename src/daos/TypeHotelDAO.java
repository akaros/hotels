package daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import beans.TypeHotel;
import core.DAO;
import core.List;
import tools.ConnectionManager;

public class TypeHotelDAO implements DAO<TypeHotel> {

	@Override
	public TypeHotel create(TypeHotel object) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TypeHotel find(Object id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TypeHotel> findAll() throws Exception {
		
		List<TypeHotel> results = new List<TypeHotel>();
		Statement statement = null;
		
		try {
			
			statement = ConnectionManager.getConnection().createStatement();
			ResultSet rs = statement.executeQuery("select nutype, nomtype from tpe order by nutype");
			while( rs.next() ) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				results.add(new TypeHotel(id, name));
			}
			rs.close();
			
		} catch (SQLException e ) {
			System.out.println(e);
		}
		
		statement.close();
		return results;
	}

	@Override
	public TypeHotel update(TypeHotel object) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(TypeHotel object) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
