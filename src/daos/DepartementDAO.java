package daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import beans.Departement;
import core.DAO;
import core.List;
import tools.ConnectionManager;

/* DEPARTEMENT DAO */

public class DepartementDAO implements DAO<Departement> {
	
	/* METHODS */
	
	@Override
	public Departement create(Departement object) throws Exception {
		return null;
	}

	@Override
	public Departement find(Object id) throws Exception {
		return null;
	}

	@Override
	public List<Departement> findAll() throws SQLException {
			
		List<Departement> results = new List<Departement>();
		/*Statement statement = null;
			
		try {
			statement = ConnectionManager.getConnection().createStatement();
			ResultSet rs = statement.executeQuery("select distinct * from (select substr(codepost, 0, 2) \"DEP\" from hotel) order by dep asc");
			while( rs.next() ) {
				int code = Integer.parseInt(rs.getString(1));
				results.add(new Departement(code));
			}
			rs.close();
		} catch (Exception e ) {
			System.out.println(e);
		}
			
		statement.close();*/
		for( int code = 1; code < 96; code++ ) {
			results.add(new Departement(code));
		}
		return results;
	}

	@Override
	public Departement update(Departement object) throws Exception {
		return null;
	}

	@Override
	public void delete(Departement object) throws Exception {
		
	}

}
