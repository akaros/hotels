package daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import beans.CategorieChambre;
import core.DAO;
import core.List;
import tools.ConnectionManager;

public class CategorieChambreDAO implements DAO<CategorieChambre> {

	@Override
	public CategorieChambre create(CategorieChambre object) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CategorieChambre find(Object id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CategorieChambre> findAll() throws Exception {
		
		List<CategorieChambre> results = new List<CategorieChambre>();
		Statement statement = null;
		
		try {
			
			statement = ConnectionManager.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from cat order by nucat");
			while( resultSet.next() ) {
				results.add(new CategorieChambre(resultSet.getInt(1), resultSet.getString(2)));
			}
			resultSet.close();
			
		} catch (SQLException e ) {
			System.out.println(e);
		}
		
		statement.close();
		return results;
	}

	@Override
	public CategorieChambre update(CategorieChambre object) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(CategorieChambre object) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
