package tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	
	// Attributes
	
	private static Connection connection;	
	public static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";
	public static final String ORACLE_URL = "jdbc:oracle:thin:@localhost:1521:xe";	

	// Constructor
	
	public static boolean init(String user, String password) {
		
		try {
			System.out.println("Connection at " + ConnectionManager.ORACLE_URL);
			Class.forName(ORACLE_DRIVER);
			connection = DriverManager.getConnection(ORACLE_URL, user, password);
			return true;
		} catch( ClassNotFoundException e) {
			e.printStackTrace();
		} catch( SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static Connection getConnection() {
		
		return connection;
	}
}
