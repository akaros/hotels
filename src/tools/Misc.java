package tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Misc {
	
	// Convertion String[] en ArrayList<Integer>
    public static ArrayList<Integer> convertToIntArray(String[] strArray) throws Exception {
    	
    	ArrayList<Integer> intArray = new ArrayList<Integer>();
    	
    	if( strArray != null ) {
	    	for( String str : strArray ) {
	    		intArray.add(new Integer(Integer.parseInt(str)));
	    	}
    	}
    	return intArray;
    }
	
	// Pr�pare la requ�te SQL avec les objets pass�s en param�tre
	public static PreparedStatement initPreparedStatement(Connection connexion, String sql, boolean returnGeneratedKeys, Object... objets) throws SQLException {
		    
		PreparedStatement preparedStatement = connexion.prepareStatement( sql, returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS );
		for ( int i = 0; i < objets.length; i++ ) {
			preparedStatement.setObject( i + 1, objets[i] );
		}
		return preparedStatement;
	}
	
	public static String titleCase(String str) {
		
		return str.substring(0, 1).toUpperCase() + str.substring(1, str.length()).toLowerCase();
	}
}
