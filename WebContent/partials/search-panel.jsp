<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="search-panel">
	<form action="/Hotels/search" method="post">
		<div class="box">
			<div class="title">Classement</div>
			<div class="content">
				<c:forEach var="index" begin="0" end="${types.size()-1}" >
				  <div class="choice">
				  	<span class="check"><input type='checkbox' name='typs' value="${types.get(index).getId()}"><c:out value="${types.get(index).getName()}"/></span>
				  	<span class="quantity"><b><c:out value="${typesCount.get(index)}"/></b></span>
				  </div>
				</c:forEach>
			</div>
		</div>
		<div class="box">
			<div class="title">Cat. des chambres</div>
			<div class="content">
				<c:forEach var="index" begin="0" end="${categories.size()-1}" >
				  <div class="choice">
				  	<span class="check"><input type='checkbox' name='cats' value="${categories.get(index).getId()}"><c:out value="${categories.get(index).getName()}"/></span>
				  	<span class="quantity"><b><c:out value="${categoriesCount.get(index)}"/></b></span>
				  </div>
				</c:forEach>
			</div>
		</div>
		<div class="box deps">
			<div class="title">Départements</div>
			<div class="content">
				<c:forEach var="index" begin="0" end="${departements.size()-1}" >
				  <div class="choice">
				  	<span class="check"><input type='checkbox' name='deps' value="${departements.get(index).getId()}"><c:out value="${departements.get(index).getName()}"/></span>
				  	<span class="quantity"><b><c:out value="${departementsCount.get(index)}"/></b></span>
				  </div>
				</c:forEach>
			</div>
		</div>
		<button class="button" type="submit">Rechercher</button>
	</form>
</div>