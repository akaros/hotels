<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="results-panel">
	<table>
		<tr>
			<th>#</th>
			<th>Nom</th> 
			<th>Classement</th>
			<th>Adresse</th>
			<th>Code postal</th>
			<th>Ville</th>
			<th>Voir</th>
		</tr>
		<c:forEach var="i" begin="${start}" end="${start+99}" >
			<tr>
				<td><c:out value="${sessionScope.results.get(i).getNum()}"/></td>
				<td><c:out value="${sessionScope.results.get(i).getNom()}"/></td>
				<td><c:out value="${sessionScope.results.get(i).getType()}"/></td>
				<td><c:out value="${sessionScope.results.get(i).getAdresse()}"/></td>
				<td><c:out value="${sessionScope.results.get(i).getCP()}"/></td>
				<td><c:out value="${Scope.results.get(i).getVille()}"/></td>
				<td><a href="/Hotels/search?num=${sessionScope.results.get(i).getNum()}">Fiche</a></td>
			</tr>
		</c:forEach>
	</table>
</div>