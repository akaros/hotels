<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="top-panel">
	<div class="nb-results">
		<c:out value="${sessionScope.results.size()}"/> résultats
		<c:if test="${page>1}">
			<a href="/Hotels/search?page=${page-1}">Précedent</a>
		</c:if>
		<a href="/Hotels/search?page=${page+1}">Suivant</a>
		<c:out value="${start}"/>
	</div>
</div>