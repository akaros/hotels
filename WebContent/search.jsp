<%@ page import="java.util.Map, java.util.Set, java.util.Map.Entry" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="beans.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Booking hôtels</title>
		<link type="text/css" rel="stylesheet" href="<c:url value="/styles/search.css" />"/>
	</head>
	<body>
		<jsp:include page="/partials/title-panel.jsp"/>
		<jsp:include page="/partials/top-panel.jsp"/>
		<jsp:include page="/partials/user-panel.jsp"/>
		<jsp:include page="/partials/search-panel.jsp"/>
		<jsp:include page="/partials/results-panel.jsp"/>
		<jsp:include page="/partials/hotel-panel.jsp"/>
	</body>
</html>